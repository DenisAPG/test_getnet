package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.concurrent.TimeUnit;

public class GetnetPage extends BasePage {

    private String getNetURI = "https://site.getnet.com.br/";
    private String searchTriggerID = "search-trigger";
    private String cssSelectorSearchTrigger = "#search-trigger > div";
    private String globalSearchID = "global-search-input";

    //cssSelectors for "Posso utilizar a conta superget para receber as vendas de outras maquininhas?" link.
    private String cssSelectorlinkResultModal= "body > div.o-modal.is-modal-open > div";
    private String cssSelectorContaSupergetOutrasMaquininhas = "body > div.c-wrapper > div > section > a:nth-child(12)";
    private String cssSelectorTextModalContaSupergetOutrasMaquininhas = "body > div.o-modal.is-modal-open > div > div.o-modal__text > div";

    public GetnetPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void getGetnetPage(){
        webDriver.get(getNetURI);
    }

    public void clickOnSearchTrigger() {
        waitForElementToBeClickableByCssSelector(cssSelectorSearchTrigger);
        waitForElementToBeVisibleByCssSelector(cssSelectorSearchTrigger);
        Sleep(10);
        webDriver.findElement(By.cssSelector(cssSelectorSearchTrigger)).click();
    }

    public void fillGlobalSearch(String searchInfo) {
        waitForElementToBeClickableByID(globalSearchID);
        webDriver.findElement(By.id(globalSearchID)).sendKeys(searchInfo, Keys.ENTER);
    }

    public void waitForSearchResultsVisibility(){
        waitForElementToBeClickableByCssSelector(cssSelectorContaSupergetOutrasMaquininhas);
    }

    public boolean isLinkResultModalOpen() {
        waitForElementToBeVisibleByCssSelector(cssSelectorlinkResultModal);
        return webDriver.findElement(By.cssSelector(cssSelectorlinkResultModal)).isDisplayed();
    }

    public String extractLinkResultModalModalText(){
        return webDriver.findElement(By.cssSelector(cssSelectorTextModalContaSupergetOutrasMaquininhas)).getText();
    }
}
