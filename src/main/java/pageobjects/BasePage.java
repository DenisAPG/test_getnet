package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BasePage {

    protected static WebDriver webDriver;

    //creating default WebDriverWait
    protected static WebDriverWait wait;

    public BasePage(WebDriver webDriver) {
        BasePage.webDriver = webDriver;
        wait = new WebDriverWait(BasePage.webDriver, 30);
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void setWebDriver(WebDriver webDriver) {
        BasePage.webDriver = webDriver;
    }

    protected void waitForElementToBeClickableByID(String id) {
        wait.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(id))));
    }

    protected void waitForElementToBeClickableByName(String name) {
        wait.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.name(name))));
    }

    protected void waitForElementToBeClickableByCssSelector(String cssSelector) {
        wait.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.cssSelector(cssSelector))));
    }

    protected void waitForElementToBeVisibleByCssSelector(String CssSelector) {
        wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.cssSelector(CssSelector))));
    }

    protected void waitForElementToBeVisibleByID(String id) {
        wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.id(id))));
    }

    public void clickOnLinkSearchResult(String expectedLinkText){
        wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.linkText(expectedLinkText))));
        webDriver.findElement(By.linkText(expectedLinkText)).click();
        Sleep(15);
    }

    protected static void Sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            System.out.println("got interrupted!");
        }
    }

    protected static void implicitWait(int seconds){
        webDriver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

}
