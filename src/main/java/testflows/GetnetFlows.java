package testflows;


import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import pageobjects.GetnetPage;

public class GetnetFlows {

    private static GetnetPage getnetPage;
    public GetnetFlows(WebDriver webDriver){
        getnetPage = new GetnetPage(webDriver);
    }

    public void accessGetnetPageStep(){
        getnetPage.getGetnetPage();
    }

    public void searchForSupergetStep(String search){
        getnetPage.clickOnSearchTrigger();
        getnetPage.fillGlobalSearch(search);
        getnetPage.waitForSearchResultsVisibility();
    }

    public void clickOnSearchResultDesired(String expected)
    {
        getnetPage.clickOnLinkSearchResult(expected);
    }

    public void verifyModalWasDisplayedAndText(String expectedText){
        Assertions.assertTrue(getnetPage.isLinkResultModalOpen(),"Modal was not displayed!");
        String text = getnetPage.extractLinkResultModalModalText();
        Assertions.assertEquals(expectedText,text);
    }
}
