package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public final class WebDriverFactory {

    private WebDriverFactory(){
    }

    public static WebDriver createChromeDriver(String path){
        System.out.println("Initializing Chrome webDriver...");
        System.setProperty("webdriver.chrome.driver", path);
        return new ChromeDriver();
    }
}
