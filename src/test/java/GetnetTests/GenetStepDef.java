package GetnetTests;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import testflows.GetnetFlows;
import utils.WebDriverFactory;

public class GenetStepDef {

    public static WebDriver webDriver;
    public static GetnetFlows getnetFlows;

    @Before
    public void cucumberInitialize(){
        webDriver = WebDriverFactory.createChromeDriver("src/main/resources/chromedriver.exe");
        getnetFlows = new GetnetFlows(webDriver);
    }

    @After
    public void cucumberFinalize(){
        System.out.println("Closing webDriver...");
        webDriver.close();
        webDriver.quit();
    }


    @BeforeAll
    static void initialize(){

        webDriver = WebDriverFactory.createChromeDriver("src/main/resources/chromedriver.exe");
        getnetFlows = new GetnetFlows(webDriver);
    }

    @AfterAll
    static void finalizeTest(){
        System.out.println("Closing webDriver...");
        webDriver.close();
        webDriver.quit();
    }

    @Test
    public void getnetChallenge() {
        getnetFlows.accessGetnetPageStep();
        getnetFlows.searchForSupergetStep("superget");
        String expected = "Posso utilizar a conta SuperGet para receber as vendas de outras maquininhas?";
        getnetFlows.clickOnSearchResultDesired(expected);
        String expectedText = "Ainda não, você pode utilizar sua Conta SuperGet apenas para receber suas vendas " +
                "das máquinas SuperGet, receber e realizar transferências entre qualquer banco.";
        getnetFlows.verifyModalWasDisplayedAndText(expectedText);
    }

    @Given("I am able to access site.getnet.com.br")
    public void iAmAbleToAccessWebPage() {
        getnetFlows.accessGetnetPageStep();
    }

    @When("I search for {string}")
    public void iSearchForSearchVariable(String search) {
        getnetFlows.searchForSupergetStep(search);

    }

    @And("Click on {string}")
    public void clickOnExpectedSearchResult(String expected) {
//        String expected = "Posso utilizar a conta SuperGet para receber as vendas de outras maquininhas?";
        getnetFlows.clickOnSearchResultDesired(expected);
    }

    @Then("I should receive a Model with the text: {string}")
    public void iShouldReceiveAModelWithTheTextExpectedText(String expectedText) {
//        String expectedText = "Ainda não, você pode utilizar sua Conta SuperGet apenas para receber suas vendas " +
//                "das máquinas SuperGet, receber e realizar transferências entre qualquer banco.";
        getnetFlows.verifyModalWasDisplayedAndText(expectedText);
    }
}
