Feature:Getnet test

  Scenario Outline: Desafio Automação
    Given I am able to access site.getnet.com.br
    When I search for "<search variable>"
    And Click on "<expected search result>"
    Then I should receive a Model with the text: "<expected text>"
    Examples:
    |search variable|expected search result|expected text|
    |superget|Posso utilizar a conta SuperGet para receber as vendas de outras maquininhas?|Ainda não, você pode utilizar sua Conta SuperGet apenas para receber suas vendas das máquinas SuperGet, receber e realizar transferências entre qualquer banco.|